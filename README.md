Client server application. The backend is made using Quarkus while the frontend was developed using Angular. The goal is to keep track of the games played during a Championship. 

Requirements:
-Postgres
-Java
-Maven 
-Node

Installation:
mvn install
in the fe directory run: npm install
                         ng serve

