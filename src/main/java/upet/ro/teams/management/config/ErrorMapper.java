
package upet.ro.teams.management.config;

import io.quarkus.runtime.annotations.RegisterForReflection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.core.Response;
import java.io.Serializable;

/**
 * Error mapper for the application
 */
@Provider
public class ErrorMapper implements ExceptionMapper<Exception> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorMapper.class);

    @RegisterForReflection
    public class ErrorReponse implements Serializable {
        public String error;
        public Integer code;

        public ErrorReponse(String errorMessage, Integer code) {
            this.error = errorMessage;
            this.code = code;
        }
    }

    @Override
    public Response toResponse(Exception exception) {

        int code = 500; 
        if (exception instanceof WebApplicationException) {
            code = ((WebApplicationException) exception).getResponse().getStatus();
        }

        // internal server error
        if(code == 500) {
            LOGGER.error("", exception);
        }

        // not founc
        else if(code == 404) {
            LOGGER.info(exception.toString());
        }
        else {
            LOGGER.warn(exception.getMessage());
        }

        return Response.status(code)
                .entity(new ErrorReponse(exception.getMessage(), code))
                .build();
    }

}
