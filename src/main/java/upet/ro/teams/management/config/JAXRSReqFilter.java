
package upet.ro.teams.management.config;

import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Provider
@QueryParamValidator
public class JAXRSReqFilter implements ContainerRequestFilter {

    @Context
    ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        Set<String> queryParamKeys = requestContext.getUriInfo().getQueryParameters().keySet();

        Annotation[][] annotations = resourceInfo.getResourceMethod().getParameterAnnotations();

        List<String> queryParamNames = Arrays.stream(annotations)
                .flatMap(Arrays::stream)
                .filter((x) -> x instanceof QueryParam)
                .map((x) -> ((QueryParam) x).value())
                .collect(Collectors.toList()
        );

        if(queryParamKeys.size() > queryParamNames.size()){
            throw new WebApplicationException("Invalid number of query parameters. Expected at most " + queryParamNames.size() + " but got " + queryParamKeys.size(), Response.Status.BAD_REQUEST);
        }

        for(String queryParam: queryParamKeys){
            if(!queryParamNames.contains(queryParam)){
                requestContext.abortWith(Response
                        .status(Response.Status.BAD_REQUEST)
                        .entity("Invalid Query Param " + queryParam)
                        .build()
                );
                return;
            }
        }
    }

}
    