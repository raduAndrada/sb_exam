
package upet.ro.teams.management.repository;

import io.quarkus.panache.common.Page;
import upet.ro.teams.management.domain.Championship;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.domain.common.Pagination;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ChampionshipRepository implements PanacheRepository<Championship> {
    
    public int update(Championship updated) {
         return update( "name =?1, prizePool =?2, startDate =?3, endDate =?4, noOfGames =?5 where id is ?6",updated.name, updated.prizePool, updated.startDate, updated.endDate, updated.noOfGames,  updated.id );
     }

    public EntityPage<Championship> getPage(final Pagination pagination) {
        PanacheQuery<Championship> entityList ;
        PanacheQuery<Championship> page ;
        EntityPage entityPage =  new EntityPage();
        if (pagination != null ) {
            if (pagination.getSortBy() != null  && !pagination.getSortBy().isEmpty()) {
                entityList = findAll(Sort.by(pagination.getSortBy(), pagination.getSortDirection()));
            } else {
                entityList = findAll();
            }
            int pageNumber = pagination.getPageNumber() > 0 ? pagination.getPageNumber() - 1 : pagination.getPageNumber();
            page = entityList.page(Page.of(pageNumber * pagination.getPageSize() , pagination.getPageSize()));
            pagination.setPageCount(entityList.pageCount());
            entityPage.setPagination(pagination);
        } else {
            page = findAll();
            entityPage.setPagination(new Pagination()
                                        .setPageCount(1)
                                        .setPageNumber(1)
                                        .setPageSize(page.list().size() > 0 ? page.list().size() : 10)
                                        .setSortBy("")
                                        .setSortDirection(Sort.Direction.Ascending));
        }
        entityPage.setEntityPage(page.list());
        return entityPage;
    }
}
