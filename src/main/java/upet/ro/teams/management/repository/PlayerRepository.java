
package upet.ro.teams.management.repository;

import upet.ro.teams.management.domain.Player;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.domain.common.Pagination;
import io.quarkus.panache.common.Page;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class PlayerRepository implements PanacheRepository<Player> {
    
    public int update(Player updated) {
         return update( "name =?1, phoneNumber =?2, address =?3, efficiency =?4, annualSalary =?5, position =?6,  where id is ?8",updated.name, updated.phoneNumber, updated.address, updated.efficiency, updated.annualSalary, updated.position,  updated.id );
     }

    public EntityPage<Player> getPage(final Pagination pagination) {
        PanacheQuery<Player> entityList ;
        PanacheQuery<Player> page ;
        EntityPage entityPage =  new EntityPage();
        if (pagination != null ) {
            if (pagination.getSortBy() != null  && !pagination.getSortBy().isEmpty()) {
                entityList = findAll(Sort.by(pagination.getSortBy(), pagination.getSortDirection()));
            } else {
                entityList = findAll();
            }
            int pageNumber = pagination.getPageNumber() > 0 ? pagination.getPageNumber() - 1 : pagination.getPageNumber();
            page = entityList.page(Page.of(pageNumber * pagination.getPageSize() , pagination.getPageSize()));
            pagination.setPageCount(entityList.pageCount());
            entityPage.setPagination(pagination);
        } else {
            page = findAll();
            entityPage.setPagination(new Pagination()
                                        .setPageCount(1)
                                        .setPageNumber(1)
                                        .setPageSize(page.list().size() > 0 ? page.list().size() : 10)
                                        .setSortBy("")
                                        .setSortDirection(Sort.Direction.Ascending));
        }
        entityPage.setEntityPage(page.list());
        return entityPage;
    }

    public List<Player> findByTeamId(final Long teamId) {
        return find("team.id", teamId).list();
    }
}
