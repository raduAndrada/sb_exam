
package upet.ro.teams.management.repository;

import io.quarkus.panache.common.Page;
import upet.ro.teams.management.domain.Game;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.domain.common.Pagination;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class GameRepository implements PanacheRepository<Game> {
    
    public int update(Game updated) {
         return update( "datetime =?1, score =?2, gameNumber =?3,  where id is ?4",updated.datetime, updated.score, updated.gameNumber,  updated.id );
     }

    public EntityPage<Game> getPage(final Pagination pagination) {
        PanacheQuery<Game> entityList ;
        PanacheQuery<Game> page ;
        EntityPage entityPage =  new EntityPage();
        if (pagination != null ) {
            if (pagination.getSortBy() != null  && !pagination.getSortBy().isEmpty()) {
                entityList = findAll(Sort.by(pagination.getSortBy(), pagination.getSortDirection()));
            } else {
                entityList = findAll();
            }
            int pageNumber = pagination.getPageNumber() > 0 ? pagination.getPageNumber() - 1 : pagination.getPageNumber();
            page = entityList.page(Page.of(pageNumber * pagination.getPageSize() , pagination.getPageSize()));
            pagination.setPageCount(entityList.pageCount());
            entityPage.setPagination(pagination);
        } else {
            page = findAll();
            entityPage.setPagination(new Pagination()
                                        .setPageCount(1)
                                        .setPageNumber(1)
                                        .setPageSize(page.list().size() > 0 ? page.list().size() : 10)
                                        .setSortBy("")
                                        .setSortDirection(Sort.Direction.Ascending));
        }
        entityPage.setEntityPage(page.list());
        return entityPage;
    }

    public List<Game> findByTeamId(final Long teamId) {
        return find("hostTeam.id = ?1 or guestTeam.id = ?1", teamId).list();
    }
}
