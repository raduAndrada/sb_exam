
package upet.ro.teams.management.repository;

import io.quarkus.panache.common.Page;
import upet.ro.teams.management.domain.TeamGameStats;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.domain.common.Pagination;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TeamGameStatsRepository implements PanacheRepository<TeamGameStats> {
    
    public int update(TeamGameStats updated) {
         return update( "efficiency =?1, fastbreakGoals =?2, mvp =?3, ballPosetion =?4, goalkeeperEfficiency =?5,  where id is ?6",updated.efficiency, updated.fastbreakGoals, updated.mvp, updated.ballPossession, updated.goalkeeperEfficiency,  updated.id );
     }

    public EntityPage<TeamGameStats> getPage(final Pagination pagination) {
        PanacheQuery<TeamGameStats> entityList ;
        PanacheQuery<TeamGameStats> page ;
        EntityPage entityPage =  new EntityPage();
        if (pagination != null ) {
            if (pagination.getSortBy() != null  && !pagination.getSortBy().isEmpty()) {
                entityList = findAll(Sort.by(pagination.getSortBy(), pagination.getSortDirection()));
            } else {
                entityList = findAll();
            }
            int pageNumber = pagination.getPageNumber() > 0 ? pagination.getPageNumber() - 1 : pagination.getPageNumber();
            page = entityList.page(Page.of(pageNumber * pagination.getPageSize() , pagination.getPageSize()));
            pagination.setPageCount(entityList.pageCount());
            entityPage.setPagination(pagination);
        } else {
            page = findAll();
            entityPage.setPagination(new Pagination()
                                        .setPageCount(1)
                                        .setPageNumber(1)
                                        .setPageSize(page.list().size() > 0 ? page.list().size() : 10)
                                        .setSortBy("")
                                        .setSortDirection(Sort.Direction.Ascending));
        }
        entityPage.setEntityPage(page.list());
        return entityPage;
    }

    public Optional<TeamGameStats> findByTeamIdAndGameId(final Long teamId, final Long gameId) {
        return find("team.id =?1 and game.id = ?2", teamId, gameId).firstResultOptional();
    }
}
