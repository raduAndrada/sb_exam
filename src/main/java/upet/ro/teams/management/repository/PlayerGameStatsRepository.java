
package upet.ro.teams.management.repository;

import io.quarkus.panache.common.Page;
import upet.ro.teams.management.domain.PlayerGameStats;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.domain.common.Pagination;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PlayerGameStatsRepository implements PanacheRepository<PlayerGameStats> {
    
    public int update(PlayerGameStats updated) {
         return update( "efficiency =?1, noOfGoals =?2, noOfFaults =?3, noOfAssists =?4, noOfUnprovokedMistakes =?5, minutesPlayed =?6,  where id is ?7",updated.efficiency, updated.noOfGoals, updated.noOfFaults, updated.noOfAssists, updated.noOfUnprovokedMistakes, updated.minutesPlayed,  updated.id );
     }

    public EntityPage<PlayerGameStats> getPage(final Pagination pagination) {
        PanacheQuery<PlayerGameStats> entityList ;
        PanacheQuery<PlayerGameStats> page ;
        EntityPage entityPage =  new EntityPage();
        if (pagination != null ) {
            if (pagination.getSortBy() != null  && !pagination.getSortBy().isEmpty()) {
                entityList = findAll(Sort.by(pagination.getSortBy(), pagination.getSortDirection()));
            } else {
                entityList = findAll();
            }
            int pageNumber = pagination.getPageNumber() > 0 ? pagination.getPageNumber() - 1 : pagination.getPageNumber();
            page = entityList.page(Page.of(pageNumber * pagination.getPageSize() , pagination.getPageSize()));
            pagination.setPageCount(entityList.pageCount());
            entityPage.setPagination(pagination);
        } else {
            page = findAll();
            entityPage.setPagination(new Pagination()
                                        .setPageCount(1)
                                        .setPageNumber(1)
                                        .setPageSize(page.list().size() > 0 ? page.list().size() : 10)
                                        .setSortBy("")
                                        .setSortDirection(Sort.Direction.Ascending));
        }
        entityPage.setEntityPage(page.list());
        return entityPage;
    }

    public List<PlayerGameStats> findByPlayerId(final Long playerId) {
        return find("player.id", playerId).list();
    }
}
