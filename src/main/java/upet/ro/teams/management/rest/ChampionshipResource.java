

package upet.ro.teams.management.rest;

import upet.ro.teams.management.domain.Championship;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.service.ChampionshipService;
import io.quarkus.panache.common.Sort;

import upet.ro.teams.management.config.QueryParamValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import javax.ws.rs.core.Context;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;



/**
 * Rest endpoint for Championship
 */
@Path("/api/championship")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@QueryParamValidator
public class ChampionshipResource {

    private final Logger log = LoggerFactory.getLogger(ChampionshipResource.class);

    @Inject
    ChampionshipService championshipService;

    /**
     * Find championship by id
     * @param id the identifier of the searched championship
     * @return the request with the corresponding championship if it exists
     */
    @GET
    @Path("/id/{id}")
    public Championship findById(final @PathParam("id") Long id) {
        log.info("REST request to get one Championship entity by id {}", id);
        return championshipService.findById(id)
                .orElseThrow(() -> new WebApplicationException("Resource not found", NOT_FOUND));
    }


    /**
     * Find all the instances of championship 
     * @return the list of all the instances of championship
     */
    @GET
    public List<Championship> findAll()  {
        log.info("REST request to get all Championship entities");
        return championshipService.findAll();          
    }

    /**
     * Find all the instances of championship
     * @return the list of all the instances of championship
     */
    @GET
    @Path("/page")
    public EntityPage<Championship> getPage(
            final @QueryParam("pageNumber") Integer pageNumber,
            final @QueryParam("pageSize") Integer pageSize,
            final @QueryParam("sortBy") String sortBy,
            @QueryParam("sortDirection") Sort.Direction direction
    )  {
        
        Pagination pagination = null;
        if (pageNumber != null && pageSize != null ) {
            pagination = new Pagination();
            pagination.setPageNumber(pageNumber);
            pagination.setPageSize(pageSize);
            pagination.setSortBy(sortBy);
            if (direction == null) {
                direction = Sort.Direction.Ascending;
            }
            pagination.setSortDirection(direction);
        }
        log.info("REST request to get page of Championship entities, pagination: {}", pagination);
        return championshipService.getPage(pagination);
    }

    /**
     * Create championship
     * @param championship the championship to be created
     * @return the created championship
     */
    @POST
    public Response create(final Championship championship) {
        log.info("REST request to register Championship {}", championship);
        if(championship.id != null) {
            throw new WebApplicationException("Entity has an id, and should not", BAD_REQUEST);
        }
        Championship created = championshipService.save(championship);
        return Response.ok(created).build();
    }
    
    /**
     * Update championship
     * @param championship the updated championship
     * @return the updated championship
     */
    @PUT
    public Response update(final Championship championship) {
        log.info("REST request to update Championship {}", championship);
        if(championship.id == null) {
            throw new WebApplicationException("Entity must have an id", BAD_REQUEST);
        }
        Championship retrieved = championshipService.findById(championship.id)
                .orElseThrow(()->new WebApplicationException("Entity not found", NOT_FOUND));

        championshipService.update(retrieved, championship);
        return Response.ok(retrieved).build();
    }

    /**
     * Delete championship by id
     * @param id the identifier of the championship to be deleted
     */
    @DELETE
    @Path("/id/{id}")
    public Response deleteById(final @PathParam("id") Long id) {
        log.info("REST request to DELETE one Championship entity by id {}", id);
        championshipService.delete(id);
        return Response.ok().build();
    }

}
