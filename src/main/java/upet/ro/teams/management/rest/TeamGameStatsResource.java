

package upet.ro.teams.management.rest;

import upet.ro.teams.management.domain.TeamGameStats;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.service.TeamGameStatsService;
import io.quarkus.panache.common.Sort;

import upet.ro.teams.management.config.QueryParamValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import javax.ws.rs.core.Context;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;



/**
 * Rest endpoint for TeamGameStats
 */
@Path("/api/team-game-stats")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@QueryParamValidator
public class TeamGameStatsResource {

    private final Logger log = LoggerFactory.getLogger(TeamGameStatsResource.class);

    @Inject
    TeamGameStatsService teamGameStatsService;

    /**
     * Find teamGameStats by id
     * @param id the identifier of the searched teamGameStats
     * @return the request with the corresponding teamGameStats if it exists
     */
    @GET
    @Path("/id/{id}")
    public TeamGameStats findById(final @PathParam("id") Long id) {
        log.info("REST request to get one TeamGameStats entity by id {}", id);
        return teamGameStatsService.findById(id)
                .orElseThrow(() -> new WebApplicationException("Resource not found", NOT_FOUND));
    }

    /**
     * Find teamGameStats by id
     * @param id the identifier of the searched teamGameStats
     * @return the request with the corresponding teamGameStats if it exists
     */
    @GET
    @Path("/teamId/{teamId}/gameId/{gameId}")
    public TeamGameStats findById(final @PathParam("teamId") Long teamId, final @PathParam("gameId") Long gameId) {
        log.info("REST request to get one TeamGameStats entity by teamId {} and gameId {} ", teamId, gameId);
        return teamGameStatsService.findByTeamAndGameId(teamId, gameId)
                .orElseThrow(() -> new WebApplicationException("Resource not found", NOT_FOUND));
    }



    /**
     * Find all the instances of teamGameStats 
     * @return the list of all the instances of teamGameStats
     */
    @GET
    public List<TeamGameStats> findAll()  {
        log.info("REST request to get all TeamGameStats entities");
        return teamGameStatsService.findAll();          
    }

    /**
     * Find all the instances of teamGameStats
     * @return the list of all the instances of teamGameStats
     */
    @GET
    @Path("/page")
    public EntityPage<TeamGameStats> getPage(
            final @QueryParam("pageNumber") Integer pageNumber,
            final @QueryParam("pageSize") Integer pageSize,
            final @QueryParam("sortBy") String sortBy,
            @QueryParam("sortDirection") Sort.Direction direction
    )  {
        
        Pagination pagination = null;
        if (pageNumber != null && pageSize != null ) {
            pagination = new Pagination();
            pagination.setPageNumber(pageNumber);
            pagination.setPageSize(pageSize);
            pagination.setSortBy(sortBy);
            if (direction == null) {
                direction = Sort.Direction.Ascending;
            }
            pagination.setSortDirection(direction);
        }
        log.info("REST request to get page of TeamGameStats entities, pagination: {}", pagination);
        return teamGameStatsService.getPage(pagination);
    }

    /**
     * Create teamGameStats
     * @param teamGameStats the teamGameStats to be created
     * @return the created teamGameStats
     */
    @POST
    public Response create(final TeamGameStats teamGameStats) {
        log.info("REST request to register TeamGameStats {}", teamGameStats);
        if(teamGameStats.id != null) {
            throw new WebApplicationException("Entity has an id, and should not", BAD_REQUEST);
        }
        TeamGameStats created = teamGameStatsService.save(teamGameStats);
        return Response.ok(created).build();
    }
    
    /**
     * Update teamGameStats
     * @param teamGameStats the updated teamGameStats
     * @return the updated teamGameStats
     */
    @PUT
    public Response update(final TeamGameStats teamGameStats) {
        log.info("REST request to update TeamGameStats {}", teamGameStats);
        if(teamGameStats.id == null) {
            throw new WebApplicationException("Entity must have an id", BAD_REQUEST);
        }
        TeamGameStats retrieved = teamGameStatsService.findById(teamGameStats.id)
                .orElseThrow(()->new WebApplicationException("Entity not found", NOT_FOUND));

        teamGameStatsService.update(retrieved, teamGameStats);
        return Response.ok(retrieved).build();
    }

    /**
     * Delete teamGameStats by id
     * @param id the identifier of the teamGameStats to be deleted
     */
    @DELETE
    @Path("/id/{id}")
    public Response deleteById(final @PathParam("id") Long id) {
        log.info("REST request to DELETE one TeamGameStats entity by id {}", id);
        teamGameStatsService.delete(id);
        return Response.ok().build();
    }


}
