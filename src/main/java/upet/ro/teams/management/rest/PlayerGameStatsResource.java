

package upet.ro.teams.management.rest;

import upet.ro.teams.management.domain.PlayerGameStats;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.service.PlayerGameStatsService;
import io.quarkus.panache.common.Sort;

import upet.ro.teams.management.config.QueryParamValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import javax.ws.rs.core.Context;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;



/**
 * Rest endpoint for PlayerGameStats
 */
@Path("/api/player-game-stats")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@QueryParamValidator
public class PlayerGameStatsResource {

    private final Logger log = LoggerFactory.getLogger(PlayerGameStatsResource.class);

    @Inject
    PlayerGameStatsService playerGameStatsService;

    /**
     * Find playerGameStats by id
     * @param id the identifier of the searched playerGameStats
     * @return the request with the corresponding playerGameStats if it exists
     */
    @GET
    @Path("/id/{id}")
    public PlayerGameStats findById(final @PathParam("id") Long id) {
        log.info("REST request to get one PlayerGameStats entity by id {}", id);
        return playerGameStatsService.findById(id)
                .orElseThrow(() -> new WebApplicationException("Resource not found", NOT_FOUND));
    }

    @GET
    @Path("/playerId/{playerId}")
    public List<PlayerGameStats> findByPlayerId(final @PathParam("playerId") Long playerId) {
        log.info("REST request to get one PlayerGameStats entity by playerId {}", playerId);
        return playerGameStatsService.findByPlayerId(playerId);
    }


    /**
     * Find all the instances of playerGameStats 
     * @return the list of all the instances of playerGameStats
     */
    @GET
    public List<PlayerGameStats> findAll()  {
        log.info("REST request to get all PlayerGameStats entities");
        return playerGameStatsService.findAll();          
    }

    /**
     * Find all the instances of playerGameStats
     * @return the list of all the instances of playerGameStats
     */
    @GET
    @Path("/page")
    public EntityPage<PlayerGameStats> getPage(
            final @QueryParam("pageNumber") Integer pageNumber,
            final @QueryParam("pageSize") Integer pageSize,
            final @QueryParam("sortBy") String sortBy,
            @QueryParam("sortDirection") Sort.Direction direction
    )  {
        
        Pagination pagination = null;
        if (pageNumber != null && pageSize != null ) {
            pagination = new Pagination();
            pagination.setPageNumber(pageNumber);
            pagination.setPageSize(pageSize);
            pagination.setSortBy(sortBy);
            if (direction == null) {
                direction = Sort.Direction.Ascending;
            }
            pagination.setSortDirection(direction);
        }
        log.info("REST request to get page of PlayerGameStats entities, pagination: {}", pagination);
        return playerGameStatsService.getPage(pagination);
    }

    /**
     * Create playerGameStats
     * @param playerGameStats the playerGameStats to be created
     * @return the created playerGameStats
     */
    @POST
    public Response create(final PlayerGameStats playerGameStats) {
        log.info("REST request to register PlayerGameStats {}", playerGameStats);
        if(playerGameStats.id != null) {
            throw new WebApplicationException("Entity has an id, and should not", BAD_REQUEST);
        }
        PlayerGameStats created = playerGameStatsService.save(playerGameStats);
        return Response.ok(created).build();
    }
    
    /**
     * Update playerGameStats
     * @param playerGameStats the updated playerGameStats
     * @return the updated playerGameStats
     */
    @PUT
    public Response update(final PlayerGameStats playerGameStats) {
        log.info("REST request to update PlayerGameStats {}", playerGameStats);
        if(playerGameStats.id == null) {
            throw new WebApplicationException("Entity must have an id", BAD_REQUEST);
        }
        PlayerGameStats retrieved = playerGameStatsService.findById(playerGameStats.id)
                .orElseThrow(()->new WebApplicationException("Entity not found", NOT_FOUND));

        playerGameStatsService.update(retrieved, playerGameStats);
        return Response.ok(retrieved).build();
    }

    /**
     * Delete playerGameStats by id
     * @param id the identifier of the playerGameStats to be deleted
     */
    @DELETE
    @Path("/id/{id}")
    public Response deleteById(final @PathParam("id") Long id) {
        log.info("REST request to DELETE one PlayerGameStats entity by id {}", id);
        playerGameStatsService.delete(id);
        return Response.ok().build();
    }


}
