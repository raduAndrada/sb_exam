

package upet.ro.teams.management.rest;

import upet.ro.teams.management.domain.Team;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.service.TeamService;
import io.quarkus.panache.common.Sort;

import upet.ro.teams.management.config.QueryParamValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import javax.ws.rs.core.Context;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;



/**
 * Rest endpoint for Team
 */
@Path("/api/team")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@QueryParamValidator
public class TeamResource {

    private final Logger log = LoggerFactory.getLogger(TeamResource.class);

    @Inject
    TeamService teamService;

    /**
     * Find team by id
     * @param id the identifier of the searched team
     * @return the request with the corresponding team if it exists
     */
    @GET
    @Path("/id/{id}")
    public Team findById(final @PathParam("id") Long id) {
        log.info("REST request to get one Team entity by id {}", id);
        return teamService.findById(id)
                .orElseThrow(() -> new WebApplicationException("Resource not found", NOT_FOUND));
    }



    /**
     * Find all the instances of team 
     * @return the list of all the instances of team
     */
    @GET
    public List<Team> findAll()  {
        log.info("REST request to get all Team entities");
        return teamService.findAll();          
    }

    /**
     * Find all the instances of team
     * @return the list of all the instances of team
     */
    @GET
    @Path("/page")
    public EntityPage<Team> getPage(
            final @QueryParam("pageNumber") Integer pageNumber,
            final @QueryParam("pageSize") Integer pageSize,
            final @QueryParam("sortBy") String sortBy,
            @QueryParam("sortDirection") Sort.Direction direction
    )  {
        
        Pagination pagination = null;
        if (pageNumber != null && pageSize != null ) {
            pagination = new Pagination();
            pagination.setPageNumber(pageNumber);
            pagination.setPageSize(pageSize);
            pagination.setSortBy(sortBy);
            if (direction == null) {
                direction = Sort.Direction.Ascending;
            }
            pagination.setSortDirection(direction);
        }
        log.info("REST request to get page of Team entities, pagination: {}", pagination);
        return teamService.getPage(pagination);
    }

    /**
     * Create team
     * @param team the team to be created
     * @return the created team
     */
    @POST
    public Response create(final Team team) {
        log.info("REST request to register Team {}", team);
        if(team.id != null) {
            throw new WebApplicationException("Entity has an id, and should not", BAD_REQUEST);
        }
        Team created = teamService.save(team);
        return Response.ok(created).build();
    }
    
    /**
     * Update team
     * @param team the updated team
     * @return the updated team
     */
    @PUT
    public Response update(final Team team) {
        log.info("REST request to update Team {}", team);
        if(team.id == null) {
            throw new WebApplicationException("Entity must have an id", BAD_REQUEST);
        }
        Team retrieved = teamService.findById(team.id)
                .orElseThrow(()->new WebApplicationException("Entity not found", NOT_FOUND));

        teamService.update(retrieved, team);
        return Response.ok(retrieved).build();
    }

    /**
     * Delete team by id
     * @param id the identifier of the team to be deleted
     */
    @DELETE
    @Path("/id/{id}")
    public Response deleteById(final @PathParam("id") Long id) {
        log.info("REST request to DELETE one Team entity by id {}", id);
        teamService.delete(id);
        return Response.ok().build();
    }

}
