

package upet.ro.teams.management.rest;

import upet.ro.teams.management.domain.Player;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.service.PlayerService;
import io.quarkus.panache.common.Sort;

import upet.ro.teams.management.config.QueryParamValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import javax.ws.rs.core.Context;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;



/**
 * Rest endpoint for Player
 */
@Path("/api/player")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@QueryParamValidator
public class PlayerResource {

    private final Logger log = LoggerFactory.getLogger(PlayerResource.class);

    @Inject
    PlayerService playerService;

    /**
     * Find player by id
     * @param id the identifier of the searched player
     * @return the request with the corresponding player if it exists
     */
    @GET
    @Path("/id/{id}")
    public Player findById(final @PathParam("id") Long id) {
        log.info("REST request to get one Player entity by id {}", id);
        return playerService.findById(id)
                .orElseThrow(() -> new WebApplicationException("Resource not found", NOT_FOUND));
    }


    @GET
    @Path("/teamId/{teamId}")
    public List<Player> findByTeamId(final @PathParam("teamId") Long teamId) {
        log.info("REST request to get one Player entity by teamId {}", teamId);
        return playerService.findByTeamId(teamId);
    }


    /**
     * Find all the instances of player 
     * @return the list of all the instances of player
     */
    @GET
    public List<Player> findAll()  {
        log.info("REST request to get all Player entities");
        return playerService.findAll();          
    }

    /**
     * Find all the instances of player
     * @return the list of all the instances of player
     */
    @GET
    @Path("/page")
    public EntityPage<Player> getPage(
            final @QueryParam("pageNumber") Integer pageNumber,
            final @QueryParam("pageSize") Integer pageSize,
            final @QueryParam("sortBy") String sortBy,
            @QueryParam("sortDirection") Sort.Direction direction
    )  {
        
        Pagination pagination = null;
        if (pageNumber != null && pageSize != null ) {
            pagination = new Pagination();
            pagination.setPageNumber(pageNumber);
            pagination.setPageSize(pageSize);
            pagination.setSortBy(sortBy);
            if (direction == null) {
                direction = Sort.Direction.Ascending;
            }
            pagination.setSortDirection(direction);
        }
        log.info("REST request to get page of Player entities, pagination: {}", pagination);
        return playerService.getPage(pagination);
    }

    /**
     * Create player
     * @param player the player to be created
     * @return the created player
     */
    @POST
    public Response create(final Player player) {
        log.info("REST request to register Player {}", player);
        if(player.id != null) {
            throw new WebApplicationException("Entity has an id, and should not", BAD_REQUEST);
        }
        Player created = playerService.save(player);
        return Response.ok(created).build();
    }
    
    /**
     * Update player
     * @param player the updated player
     * @return the updated player
     */
    @PUT
    public Response update(final Player player) {
        log.info("REST request to update Player {}", player);
        if(player.id == null) {
            throw new WebApplicationException("Entity must have an id", BAD_REQUEST);
        }
        Player retrieved = playerService.findById(player.id)
                .orElseThrow(()->new WebApplicationException("Entity not found", NOT_FOUND));

        playerService.update(retrieved, player);
        return Response.ok(retrieved).build();
    }

    /**
     * Delete player by id
     * @param id the identifier of the player to be deleted
     */
    @DELETE
    @Path("/id/{id}")
    public Response deleteById(final @PathParam("id") Long id) {
        log.info("REST request to DELETE one Player entity by id {}", id);
        playerService.delete(id);
        return Response.ok().build();
    }

}
