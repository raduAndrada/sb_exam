

package upet.ro.teams.management.rest;

import upet.ro.teams.management.domain.Game;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;
import upet.ro.teams.management.service.GameService;
import io.quarkus.panache.common.Sort;

import upet.ro.teams.management.config.QueryParamValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import javax.ws.rs.core.Context;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;



/**
 * Rest endpoint for Game
 */
@Path("/api/game")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@QueryParamValidator
public class GameResource {

    private final Logger log = LoggerFactory.getLogger(GameResource.class);

    @Inject
    GameService gameService;

    /**
     * Find game by id
     * @param id the identifier of the searched game
     * @return the request with the corresponding game if it exists
     */
    @GET
    @Path("/id/{id}")
    public Game findById(final @PathParam("id") Long id) {
        log.info("REST request to get one Game entity by id {}", id);
        return gameService.findById(id)
                .orElseThrow(() -> new WebApplicationException("Resource not found", NOT_FOUND));
    }

    @GET
    @Path("/teamId/{teamId}")
    public List<Game> findByTeamId(final @PathParam("teamId") Long teamId) {
        log.info("REST request to get one Game entity by teamId {}", teamId);
      return gameService.findByTeamId(teamId);
    }


    /**
     * Find all the instances of game 
     * @return the list of all the instances of game
     */
    @GET
    public List<Game> findAll()  {
        log.info("REST request to get all Game entities");
        return gameService.findAll();          
    }

    /**
     * Find all the instances of game
     * @return the list of all the instances of game
     */
    @GET
    @Path("/page")
    public EntityPage<Game> getPage(
            final @QueryParam("pageNumber") Integer pageNumber,
            final @QueryParam("pageSize") Integer pageSize,
            final @QueryParam("sortBy") String sortBy,
            @QueryParam("sortDirection") Sort.Direction direction
    )  {
        
        Pagination pagination = null;
        if (pageNumber != null && pageSize != null ) {
            pagination = new Pagination();
            pagination.setPageNumber(pageNumber);
            pagination.setPageSize(pageSize);
            pagination.setSortBy(sortBy);
            if (direction == null) {
                direction = Sort.Direction.Ascending;
            }
            pagination.setSortDirection(direction);
        }
        log.info("REST request to get page of Game entities, pagination: {}", pagination);
        return gameService.getPage(pagination);
    }

    /**
     * Create game
     * @param game the game to be created
     * @return the created game
     */
    @POST
    public Response create(final Game game) {
        log.info("REST request to register Game {}", game);
        if(game.id != null) {
            throw new WebApplicationException("Entity has an id, and should not", BAD_REQUEST);
        }
        Game created = gameService.save(game);
        return Response.ok(created).build();
    }
    
    /**
     * Update game
     * @param game the updated game
     * @return the updated game
     */
    @PUT
    public Response update(final Game game) {
        log.info("REST request to update Game {}", game);
        if(game.id == null) {
            throw new WebApplicationException("Entity must have an id", BAD_REQUEST);
        }
        Game retrieved = gameService.findById(game.id)
                .orElseThrow(()->new WebApplicationException("Entity not found", NOT_FOUND));

        gameService.update(retrieved, game);
        return Response.ok(retrieved).build();
    }

    /**
     * Delete game by id
     * @param id the identifier of the game to be deleted
     */
    @DELETE
    @Path("/id/{id}")
    public Response deleteById(final @PathParam("id") Long id) {
        log.info("REST request to DELETE one Game entity by id {}", id);
        gameService.delete(id);
        return Response.ok().build();
    }


}
