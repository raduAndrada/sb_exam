
package upet.ro.teams.management.service;

import upet.ro.teams.management.domain.Game;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;

import upet.ro.teams.management.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Service for Game
 */
@ApplicationScoped
public class GameService {

    private final Logger log = LoggerFactory.getLogger(GameService.class);

    @Inject
    GameRepository gameRepository;

    /**
     * Save an instance of game
     * @param game the game to be saved
     * @return the saved game
     */
    @Transactional
    public Game save(Game game) {
        log.info("Request to save Game {}", game);

        game.setModifiedAt(Instant.now());
        if(game.createdDate == null) {
            game.createdDate = game.modifiedAt;
        }

        if(game.id == null) {
            gameRepository.persist(game);
        }
        return game;
    }

   /**
     * Update an entity of game
     * @param game the game for update
     * @param other updated game
     * @return the updated game
     */
    @Transactional
    public Game update(Game game, Game other) {
        log.info("Request to update Game {}", game);
        game.setModifiedAt(Instant.now());
        game.updateValues(other);
        gameRepository.update(game);
        return game;
    }

    /**
     * Find all game
     * @return the list of all the instances of game
     */
    public List<Game> findAll() {
        log.info("Request to find all Game instances");
        return gameRepository.listAll();
    }

    /**
     * Find game by id
     * @param id the identifier of the game
     * @return the requested game
     */
    public Optional<Game> findById(Long id) {
        log.info("Request to find Game by Id {}", id);
        return gameRepository.findByIdOptional(id);
    }

    /**
     * Delete game by id
     * @param id the id of the game to be deleted
     */
    @Transactional
    public void delete(Long id) {
        log.info("Request to DELETE Game by Id {}", id);
        Optional<Game> gameOptional = this.findById(id);
        gameOptional.ifPresent(game -> gameRepository.delete(game));
    }

    /**
     * Get a page of Game entities
     * @param pagination the pagination for the page
     * @return the page of Game entities with pagination
     */
    public EntityPage<Game> getPage(final Pagination pagination) {
        log.info("Request to get a page of Game instances, pagination: {}", pagination);
        return gameRepository.getPage(pagination);
    }


    public List<Game> findByTeamId(final Long teamId) {
        return gameRepository.findByTeamId(teamId);
    }
}
