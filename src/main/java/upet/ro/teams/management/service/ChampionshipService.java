
package upet.ro.teams.management.service;

import upet.ro.teams.management.domain.Championship;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;

import upet.ro.teams.management.repository.ChampionshipRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Service for Championship
 */
@ApplicationScoped
public class ChampionshipService {

    private final Logger log = LoggerFactory.getLogger(ChampionshipService.class);

    @Inject
    ChampionshipRepository championshipRepository;

    /**
     * Save an instance of championship
     * @param championship the championship to be saved
     * @return the saved championship
     */
    @Transactional
    public Championship save(Championship championship) {
        log.info("Request to save Championship {}", championship);

        championship.setModifiedAt(Instant.now());
        if(championship.createdDate == null) {
            championship.createdDate = championship.modifiedAt;
        }

        if(championship.id == null) {
            championshipRepository.persist(championship);
        }
        return championship;
    }

   /**
     * Update an entity of championship
     * @param championship the championship for update
     * @param other updated championship
     * @return the updated championship
     */
    @Transactional
    public Championship update(Championship championship, Championship other) {
        log.info("Request to update Championship {}", championship);
        championship.setModifiedAt(Instant.now());
        championship.updateValues(other);
        championshipRepository.update(championship);
        return championship;
    }

    /**
     * Find all championship
     * @return the list of all the instances of championship
     */
    public List<Championship> findAll() {
        log.info("Request to find all Championship instances");
        return championshipRepository.listAll();
    }

    /**
     * Find championship by id
     * @param id the identifier of the championship
     * @return the requested championship
     */
    public Optional<Championship> findById(Long id) {
        log.info("Request to find Championship by Id {}", id);
        return championshipRepository.findByIdOptional(id);
    }

    /**
     * Delete championship by id
     * @param id the id of the championship to be deleted
     */
    @Transactional
    public void delete(Long id) {
        log.info("Request to DELETE Championship by Id {}", id);
        Optional<Championship> championshipOptional = this.findById(id);
        championshipOptional.ifPresent(championship -> championshipRepository.delete(championship));
    }

    /**
     * Get a page of Championship entities
     * @param pagination the pagination for the page
     * @return the page of Championship entities with pagination
     */
    public EntityPage<Championship> getPage(final Pagination pagination) {
        log.info("Request to get a page of Championship instances, pagination: {}", pagination);
        return championshipRepository.getPage(pagination);
    }
}
