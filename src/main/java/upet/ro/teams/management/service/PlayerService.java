
package upet.ro.teams.management.service;

import upet.ro.teams.management.domain.Player;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;

import upet.ro.teams.management.repository.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Service for Player
 */
@ApplicationScoped
public class PlayerService {

    private final Logger log = LoggerFactory.getLogger(PlayerService.class);

    @Inject
    PlayerRepository playerRepository;

    /**
     * Save an instance of player
     * @param player the player to be saved
     * @return the saved player
     */
    @Transactional
    public Player save(Player player) {
        log.info("Request to save Player {}", player);

        player.setModifiedAt(Instant.now());
        if(player.createdDate == null) {
            player.createdDate = player.modifiedAt;
        }

        if(player.id == null) {
            playerRepository.persist(player);
        }
        return player;
    }

   /**
     * Update an entity of player
     * @param player the player for update
     * @param other updated player
     * @return the updated player
     */
    @Transactional
    public Player update(Player player, Player other) {
        log.info("Request to update Player {}", player);
        player.setModifiedAt(Instant.now());
        player.updateValues(other);
        playerRepository.update(player);
        return player;
    }

    /**
     * Find all player
     * @return the list of all the instances of player
     */
    public List<Player> findAll() {
        log.info("Request to find all Player instances");
        return playerRepository.listAll();
    }

    /**
     * Find player by id
     * @param id the identifier of the player
     * @return the requested player
     */
    public Optional<Player> findById(Long id) {
        log.info("Request to find Player by Id {}", id);
        return playerRepository.findByIdOptional(id);
    }

    /**
     * Delete player by id
     * @param id the id of the player to be deleted
     */
    @Transactional
    public void delete(Long id) {
        log.info("Request to DELETE Player by Id {}", id);
        Optional<Player> playerOptional = this.findById(id);
        playerOptional.ifPresent(player -> playerRepository.delete(player));
    }

    /**
     * Get a page of Player entities
     * @param pagination the pagination for the page
     * @return the page of Player entities with pagination
     */
    public EntityPage<Player> getPage(final Pagination pagination) {
        log.info("Request to get a page of Player instances, pagination: {}", pagination);
        return playerRepository.getPage(pagination);
    }

    public List<Player> findByTeamId(final Long teamId) {
        return playerRepository.findByTeamId(teamId);
    }
}
