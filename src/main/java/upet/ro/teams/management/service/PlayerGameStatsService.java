
package upet.ro.teams.management.service;

import upet.ro.teams.management.domain.PlayerGameStats;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;

import upet.ro.teams.management.repository.PlayerGameStatsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Service for PlayerGameStats
 */
@ApplicationScoped
public class PlayerGameStatsService {

    private final Logger log = LoggerFactory.getLogger(PlayerGameStatsService.class);

    @Inject
    PlayerGameStatsRepository playerGameStatsRepository;

    /**
     * Save an instance of playerGameStats
     * @param playerGameStats the playerGameStats to be saved
     * @return the saved playerGameStats
     */
    @Transactional
    public PlayerGameStats save(PlayerGameStats playerGameStats) {
        log.info("Request to save PlayerGameStats {}", playerGameStats);

        playerGameStats.setModifiedAt(Instant.now());
        if(playerGameStats.createdDate == null) {
            playerGameStats.createdDate = playerGameStats.modifiedAt;
        }

        if(playerGameStats.id == null) {
            playerGameStatsRepository.persist(playerGameStats);
        }
        return playerGameStats;
    }

   /**
     * Update an entity of playerGameStats
     * @param playerGameStats the playerGameStats for update
     * @param other updated playerGameStats
     * @return the updated playerGameStats
     */
    @Transactional
    public PlayerGameStats update(PlayerGameStats playerGameStats, PlayerGameStats other) {
        log.info("Request to update PlayerGameStats {}", playerGameStats);
        playerGameStats.setModifiedAt(Instant.now());
        playerGameStats.updateValues(other);
        playerGameStatsRepository.update(playerGameStats);
        return playerGameStats;
    }

    /**
     * Find all playerGameStats
     * @return the list of all the instances of playerGameStats
     */
    public List<PlayerGameStats> findAll() {
        log.info("Request to find all PlayerGameStats instances");
        return playerGameStatsRepository.listAll();
    }

    /**
     * Find playerGameStats by id
     * @param id the identifier of the playerGameStats
     * @return the requested playerGameStats
     */
    public Optional<PlayerGameStats> findById(Long id) {
        log.info("Request to find PlayerGameStats by Id {}", id);
        return playerGameStatsRepository.findByIdOptional(id);
    }

    /**
     * Delete playerGameStats by id
     * @param id the id of the playerGameStats to be deleted
     */
    @Transactional
    public void delete(Long id) {
        log.info("Request to DELETE PlayerGameStats by Id {}", id);
        Optional<PlayerGameStats> playerGameStatsOptional = this.findById(id);
        playerGameStatsOptional.ifPresent(playerGameStats -> playerGameStatsRepository.delete(playerGameStats));
    }

    /**
     * Get a page of PlayerGameStats entities
     * @param pagination the pagination for the page
     * @return the page of PlayerGameStats entities with pagination
     */
    public EntityPage<PlayerGameStats> getPage(final Pagination pagination) {
        log.info("Request to get a page of PlayerGameStats instances, pagination: {}", pagination);
        return playerGameStatsRepository.getPage(pagination);
    }

    public List<PlayerGameStats> findByPlayerId(final Long playerId) {
        return playerGameStatsRepository.findByPlayerId(playerId);
    }
}
