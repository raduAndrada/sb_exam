
package upet.ro.teams.management.service;

import upet.ro.teams.management.domain.Team;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;

import upet.ro.teams.management.repository.TeamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Service for Team
 */
@ApplicationScoped
public class TeamService {

    private final Logger log = LoggerFactory.getLogger(TeamService.class);

    @Inject
    TeamRepository teamRepository;

    /**
     * Save an instance of team
     * @param team the team to be saved
     * @return the saved team
     */
    @Transactional
    public Team save(Team team) {
        log.info("Request to save Team {}", team);

        team.setModifiedAt(Instant.now());
        if(team.createdDate == null) {
            team.createdDate = team.modifiedAt;
        }

        if(team.id == null) {
            teamRepository.persist(team);
        }
        return team;
    }

   /**
     * Update an entity of team
     * @param team the team for update
     * @param other updated team
     * @return the updated team
     */
    @Transactional
    public Team update(Team team, Team other) {
        log.info("Request to update Team {}", team);
        team.setModifiedAt(Instant.now());
        team.updateValues(other);
        teamRepository.update(team);
        return team;
    }

    /**
     * Find all team
     * @return the list of all the instances of team
     */
    public List<Team> findAll() {
        log.info("Request to find all Team instances");
        return teamRepository.listAll();
    }

    /**
     * Find team by id
     * @param id the identifier of the team
     * @return the requested team
     */
    public Optional<Team> findById(Long id) {
        log.info("Request to find Team by Id {}", id);
        return teamRepository.findByIdOptional(id);
    }

    /**
     * Delete team by id
     * @param id the id of the team to be deleted
     */
    @Transactional
    public void delete(Long id) {
        log.info("Request to DELETE Team by Id {}", id);
        Optional<Team> teamOptional = this.findById(id);
        teamOptional.ifPresent(team -> teamRepository.delete(team));
    }

    /**
     * Get a page of Team entities
     * @param pagination the pagination for the page
     * @return the page of Team entities with pagination
     */
    public EntityPage<Team> getPage(final Pagination pagination) {
        log.info("Request to get a page of Team instances, pagination: {}", pagination);
        return teamRepository.getPage(pagination);
    }
}
