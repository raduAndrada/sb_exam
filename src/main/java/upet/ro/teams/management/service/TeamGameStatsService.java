
package upet.ro.teams.management.service;

import upet.ro.teams.management.domain.TeamGameStats;
import upet.ro.teams.management.domain.common.Pagination;
import upet.ro.teams.management.domain.common.EntityPage;

import upet.ro.teams.management.repository.TeamGameStatsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Optional;


/**
 * Service for TeamGameStats
 */
@ApplicationScoped
public class TeamGameStatsService {

    private final Logger log = LoggerFactory.getLogger(TeamGameStatsService.class);

    @Inject
    TeamGameStatsRepository teamGameStatsRepository;

    /**
     * Save an instance of teamGameStats
     * @param teamGameStats the teamGameStats to be saved
     * @return the saved teamGameStats
     */
    @Transactional
    public TeamGameStats save(TeamGameStats teamGameStats) {
        log.info("Request to save TeamGameStats {}", teamGameStats);

        teamGameStats.setModifiedAt(Instant.now());
        if(teamGameStats.createdDate == null) {
            teamGameStats.createdDate = teamGameStats.modifiedAt;
        }

        if(teamGameStats.id == null) {
            teamGameStatsRepository.persist(teamGameStats);
        }
        return teamGameStats;
    }

   /**
     * Update an entity of teamGameStats
     * @param teamGameStats the teamGameStats for update
     * @param other updated teamGameStats
     * @return the updated teamGameStats
     */
    @Transactional
    public TeamGameStats update(TeamGameStats teamGameStats, TeamGameStats other) {
        log.info("Request to update TeamGameStats {}", teamGameStats);
        teamGameStats.setModifiedAt(Instant.now());
        teamGameStats.updateValues(other);
        teamGameStatsRepository.update(teamGameStats);
        return teamGameStats;
    }

    /**
     * Find all teamGameStats
     * @return the list of all the instances of teamGameStats
     */
    public List<TeamGameStats> findAll() {
        log.info("Request to find all TeamGameStats instances");
        return teamGameStatsRepository.listAll();
    }

    /**
     * Find teamGameStats by id
     * @param id the identifier of the teamGameStats
     * @return the requested teamGameStats
     */
    public Optional<TeamGameStats> findById(Long id) {
        log.info("Request to find TeamGameStats by Id {}", id);
        return teamGameStatsRepository.findByIdOptional(id);
    }

    /**
     * Delete teamGameStats by id
     * @param id the id of the teamGameStats to be deleted
     */
    @Transactional
    public void delete(Long id) {
        log.info("Request to DELETE TeamGameStats by Id {}", id);
        Optional<TeamGameStats> teamGameStatsOptional = this.findById(id);
        teamGameStatsOptional.ifPresent(teamGameStats -> teamGameStatsRepository.delete(teamGameStats));
    }

    /**
     * Get a page of TeamGameStats entities
     * @param pagination the pagination for the page
     * @return the page of TeamGameStats entities with pagination
     */
    public EntityPage<TeamGameStats> getPage(final Pagination pagination) {
        log.info("Request to get a page of TeamGameStats instances, pagination: {}", pagination);
        return teamGameStatsRepository.getPage(pagination);
    }

    /**
     * Find teamGameStats by id
     * @param id the identifier of the teamGameStats
     * @return the requested teamGameStats
     */
    public Optional<TeamGameStats> findByTeamAndGameId(final Long teamId, final Long gameId) {
        log.info("Request to find TeamGameStats by teamId {} and gameId {}", teamId , gameId);
        return teamGameStatsRepository.findByTeamIdAndGameId(teamId,gameId);
    }
}
