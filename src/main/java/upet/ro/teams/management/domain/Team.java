
package upet.ro.teams.management.domain;

import java.util.Collection;
import java.time.Instant;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.validation.constraints.NotNull;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import javax.persistence.Entity;

@Entity
public class Team extends AbstractAuditingEntity {

    public String name;

    public String address;

    public int currentPosition;

    public String winrate;

    public String owner;

    public String coach;

    @ManyToOne
    public Championship championship;

    /**
    * @return the name
    */
    public String getName() {
        return name;
    }

    /**
    * @return the address
    */
    public String getAddress() {
        return address;
    }

    /**
    * @return the currentPosition
    */
    public int getCurrentPosition() {
        return currentPosition;
    }

    /**
    * @return the winrate
    */
    public String getWinrate() {
        return winrate;
    }


    /**
    * @return the owner
    */
    public String getOwner() {
        return owner;
    }

    /**
    * @return the coach
    */
    public String getCoach() {
        return coach;
    }

    /**
    * @return the championship
    */
    public Championship getChampionship() {
        return championship;
    }

    /**
    * @param name the name of this Team
    * @return this Team
    */
    public Team setName(final String name) {
        this.name = name;
        return this;
    }

    /**
    * @param address the address of this Team
    * @return this Team
    */
    public Team setAddress(final String address) {
        this.address = address;
        return this;
    }

    /**
    * @param currentPosition the currentPosition of this Team
    * @return this Team
    */
    public Team setCurrentPosition(final int currentPosition) {
        this.currentPosition = currentPosition;
        return this;
    }

    /**
    * @param winrate the winrate of this Team
    * @return this Team
    */
    public Team setWinrate(final String winrate) {
        this.winrate = winrate;
        return this;
    }


    /**
    * @param owner the owner of this Team
    * @return this Team
    */
    public Team setOwner(final String owner) {
        this.owner = owner;
        return this;
    }

    /**
    * @param coach the coach of this Team
    * @return this Team
    */
    public Team setCoach(final String coach) {
        this.coach = coach;
        return this;
    }

    /**
    * @param championship the championship of this Team
    * @return this Team
    */
    public Team setChampionship(final Championship championship) {
        this.championship = championship;
        return this;
    }

    @Override
    public String toString() {
        return "Team{" +
           "name='" + name + '\'' +
           ", address='" + address + '\'' +
           ", currentPosition='" + currentPosition + '\'' +
           ", winrate='" + winrate + '\'' +
           ", owner='" + owner + '\'' +
           ", coach='" + coach + '\'' +
           ", championship='" + championship + '\'' +
           '}';
    }

    public Team updateValues(final Team team) {
        return this
            .setName(team.name)
            .setAddress(team.address)
            .setCurrentPosition(team.currentPosition)
            .setWinrate(team.winrate)
            .setOwner(team.owner)
            .setCoach(team.coach)
            .setChampionship(team.championship);
    }

}
