
package upet.ro.teams.management.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class TeamGameStats extends AbstractAuditingEntity {

    public int efficiency;

    public int fastbreakGoals;

    public String mvp;

    public int ballPossession;

    public int goalkeeperEfficiency;

    @OneToOne
    public Game game;

    @OneToOne
    public Team team;

    /**
    * @return the efficiency
    */
    public int getEfficiency() {
        return efficiency;
    }

    /**
    * @return the fastbreakGoals
    */
    public int getFastbreakGoals() {
        return fastbreakGoals;
    }

    /**
    * @return the mvp
    */
    public String getMvp() {
        return mvp;
    }

    /**
    * @return the ballPosetion
    */
    public int getBallPossession() {
        return ballPossession;
    }

    /**
    * @return the goalkeeperEfficiency
    */
    public int getGoalkeeperEfficiency() {
        return goalkeeperEfficiency;
    }

    /**
    * @return the game
    */
    public Game getGame() {
        return game;
    }

    /**
    * @return the team
    */
    public Team getTeam() {
        return team;
    }

    /**
    * @param efficiency the efficiency of this TeamGameStats
    * @return this TeamGameStats
    */
    public TeamGameStats setEfficiency(final int efficiency) {
        this.efficiency = efficiency;
        return this;
    }

    /**
    * @param fastbreakGoals the fastbreakGoals of this TeamGameStats
    * @return this TeamGameStats
    */
    public TeamGameStats setFastbreakGoals(final int fastbreakGoals) {
        this.fastbreakGoals = fastbreakGoals;
        return this;
    }

    /**
    * @param mvp the mvp of this TeamGameStats
    * @return this TeamGameStats
    */
    public TeamGameStats setMvp(final String mvp) {
        this.mvp = mvp;
        return this;
    }

    /**
    * @param ballPossession the ballPosetion of this TeamGameStats
    * @return this TeamGameStats
    */
    public TeamGameStats setBallPossession(final int ballPossession) {
        this.ballPossession = ballPossession;
        return this;
    }

    /**
    * @param goalkeeperEfficiency the goalkeeperEfficiency of this TeamGameStats
    * @return this TeamGameStats
    */
    public TeamGameStats setGoalkeeperEfficiency(final int goalkeeperEfficiency) {
        this.goalkeeperEfficiency = goalkeeperEfficiency;
        return this;
    }

    /**
    * @param game the game of this TeamGameStats
    * @return this TeamGameStats
    */
    public TeamGameStats setGame(final Game game) {
        this.game = game;
        return this;
    }

    /**
    * @param team the team of this TeamGameStats
    * @return this TeamGameStats
    */
    public TeamGameStats setTeam(final Team team) {
        this.team = team;
        return this;
    }

    @Override
    public String toString() {
        return "TeamGameStats{" +
           "efficiency='" + efficiency + '\'' +
           ", fastbreakGoals='" + fastbreakGoals + '\'' +
           ", mvp='" + mvp + '\'' +
           ", ballPosetion='" + ballPossession + '\'' +
           ", goalkeeperEfficiency='" + goalkeeperEfficiency + '\'' +
           ", game='" + game + '\'' +
           ", team='" + team + '\'' +
           '}';
    }

    public TeamGameStats updateValues(final TeamGameStats teamGameStats) {
        return this
            .setEfficiency(teamGameStats.efficiency)
            .setFastbreakGoals(teamGameStats.fastbreakGoals)
            .setMvp(teamGameStats.mvp)
            .setBallPossession(teamGameStats.ballPossession)
            .setGoalkeeperEfficiency(teamGameStats.goalkeeperEfficiency)
            .setGame(teamGameStats.game)
            .setTeam(teamGameStats.team);
    }

}
