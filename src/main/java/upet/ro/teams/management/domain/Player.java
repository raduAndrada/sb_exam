
package upet.ro.teams.management.domain;

import java.util.Collection;
import java.time.Instant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Player extends AbstractAuditingEntity {

    @NotNull
    public String name;

    public String phoneNumber;

    public String address;

    public String efficiency;

    public int annualSalary;

    public String position;

    @ManyToOne
    public Team team;

    /**
    * @return the name
    */
    public String getName() {
        return name;
    }


    /**
    * @return the phoneNumber
    */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
    * @return the address
    */
    public String getAddress() {
        return address;
    }

    /**
    * @return the efficiency
    */
    public String getEfficiency() {
        return efficiency;
    }

    /**
    * @return the annualSalary
    */
    public int getAnnualSalary() {
        return annualSalary;
    }

    /**
    * @return the position
    */
    public String getPosition() {
        return position;
    }

    /**
    * @return the team
    */
    public Team getTeam() {
        return team;
    }

    /**
    * @param name the name of this Player
    * @return this Player
    */
    public Player setName(final String name) {
        this.name = name;
        return this;
    }


    /**
    * @param phoneNumber the phoneNumber of this Player
    * @return this Player
    */
    public Player setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    /**
    * @param address the address of this Player
    * @return this Player
    */
    public Player setAddress(final String address) {
        this.address = address;
        return this;
    }

    /**
    * @param efficiency the efficiency of this Player
    * @return this Player
    */
    public Player setEfficiency(final String efficiency) {
        this.efficiency = efficiency;
        return this;
    }

    /**
    * @param annualSalary the annualSalary of this Player
    * @return this Player
    */
    public Player setAnnualSalary(final int annualSalary) {
        this.annualSalary = annualSalary;
        return this;
    }

    /**
    * @param position the position of this Player
    * @return this Player
    */
    public Player setPosition(final String position) {
        this.position = position;
        return this;
    }

    /**
    * @param team the team of this Player
    * @return this Player
    */
    public Player setTeam(final Team team) {
        this.team = team;
        return this;
    }

    @Override
    public String toString() {
        return "Player{" +
           "name='" + name + '\'' +
           ", phoneNumber='" + phoneNumber + '\'' +
           ", address='" + address + '\'' +
           ", efficiency='" + efficiency + '\'' +
           ", annualSalary='" + annualSalary + '\'' +
           ", position='" + position + '\'' +
           ", team='" + team + '\'' +
           '}';
    }

    public Player updateValues(final Player player) {
        return this
            .setName(player.name)
            .setPhoneNumber(player.phoneNumber)
            .setAddress(player.address)
            .setEfficiency(player.efficiency)
            .setAnnualSalary(player.annualSalary)
            .setPosition(player.position)
            .setTeam(player.team);
    }

}
