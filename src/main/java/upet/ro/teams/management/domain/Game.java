
package upet.ro.teams.management.domain;

import java.util.Collection;
import java.time.Instant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.validation.constraints.NotNull;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import javax.persistence.Entity;

@Entity
public class Game extends AbstractAuditingEntity {

    @OneToOne
    public Team hostTeam;

    @OneToOne
    public Team guestTeam;

    public String datetime;

    public String score;

    public int gameNumber;

    @OneToOne
    public Championship championship;

    /**
    * @return the hostTeam
    */
    public Team getHostTeam() {
        return hostTeam;
    }

    /**
    * @return the guestTeam
    */
    public Team getGuestTeam() {
        return guestTeam;
    }

    /**
    * @return the datetime
    */
    public String getDatetime() {
        return datetime;
    }

    /**
    * @return the score
    */
    public String getScore() {
        return score;
    }

    /**
    * @return the gameNumber
    */
    public int getGameNumber() {
        return gameNumber;
    }

    /**
    * @return the championship
    */
    public Championship getChampionship() {
        return championship;
    }

    /**
    * @param hostTeam the hostTeam of this Game
    * @return this Game
    */
    public Game setHostTeam(final Team hostTeam) {
        this.hostTeam = hostTeam;
        return this;
    }

    /**
    * @param guestTeam the guestTeam of this Game
    * @return this Game
    */
    public Game setGuestTeam(final Team guestTeam) {
        this.guestTeam = guestTeam;
        return this;
    }

    /**
    * @param datetime the datetime of this Game
    * @return this Game
    */
    public Game setDatetime(final String datetime) {
        this.datetime = datetime;
        return this;
    }

    /**
    * @param score the score of this Game
    * @return this Game
    */
    public Game setScore(final String score) {
        this.score = score;
        return this;
    }

    /**
    * @param gameNumber the gameNumber of this Game
    * @return this Game
    */
    public Game setGameNumber(final int gameNumber) {
        this.gameNumber = gameNumber;
        return this;
    }

    /**
    * @param championship the championship of this Game
    * @return this Game
    */
    public Game setChampionship(final Championship championship) {
        this.championship = championship;
        return this;
    }

    @Override
    public String toString() {
        return "Game{" +
           "hostTeam='" + hostTeam + '\'' +
           ", guestTeam='" + guestTeam + '\'' +
           ", datetime='" + datetime + '\'' +
           ", score='" + score + '\'' +
           ", gameNumber='" + gameNumber + '\'' +
           ", championship='" + championship + '\'' +
           '}';
    }

    public Game updateValues(final Game game) {
        return this
            .setHostTeam(game.hostTeam)
            .setGuestTeam(game.guestTeam)
            .setDatetime(game.datetime)
            .setScore(game.score)
            .setGameNumber(game.gameNumber)
            .setChampionship(game.championship);
    }

}
