
package upet.ro.teams.management.domain.common;

import java.util.List;

public class EntityPage<Entity> {

	private List<Entity> entityPage;

	private Pagination pagination;


	public List<Entity> getEntityPage() {
		return entityPage;
	}

	public EntityPage setEntityPage(final List<Entity> entityPage) {
		this.entityPage = entityPage;
		return this;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public EntityPage setPagination(final Pagination pagination) {
		this.pagination = pagination;
		return this;
	}


	@Override
	public String toString() {
		return "Page{" +
				"entityPage=" + entityPage +
				", pagination=" + pagination +
				'}';
	}
}

