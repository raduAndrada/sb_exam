
package upet.ro.teams.management.domain.common;

import io.quarkus.panache.common.Sort;

public class Pagination {

	private int pageSize;

	private int pageNumber;

	private int pageCount;

	private String sortBy;

	private Sort.Direction sortDirection;

	public int getPageSize() {
		return pageSize;
	}

	public Pagination setPageSize(final int pageSize) {
		this.pageSize = pageSize;
		return this;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public Pagination setPageNumber(final int pageNumber) {
		this.pageNumber = pageNumber;
		return this;
	}

	public int getPageCount() {
		return pageCount;
	}

	public Pagination setPageCount(final int pageCount) {
		this.pageCount = pageCount;
		return this;
	}


	public String getSortBy() {
		return sortBy;
	}

	public Pagination setSortBy(final String sortBy) {
		this.sortBy = sortBy;
		return this;
	}

	public Sort.Direction getSortDirection() {
		return sortDirection;
	}

	public Pagination setSortDirection (final Sort.Direction sortDirection) {
		this.sortDirection = sortDirection;
		return this;
	}

	@Override
	public String toString() {
		return "Pagination{" +
				"pageSize=" + pageSize +
				", pageNumber=" + pageNumber +
				", totalElements=" + pageCount +
				'}';
	}

}

