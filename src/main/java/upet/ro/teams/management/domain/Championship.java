
package upet.ro.teams.management.domain;

import java.util.Collection;
import java.time.Instant;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.validation.constraints.NotNull;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import javax.persistence.Entity;

@Entity
public class Championship extends AbstractAuditingEntity {

    public String name;

    public Float prizePool;

    public String startDate;

    public String endDate;

    public int noOfGames;

    /**
    * @return the name
    */
    public String getName() {
        return name;
    }

    /**
    * @return the prizePool
    */
    public Float getPrizePool() {
        return prizePool;
    }

    /**
    * @return the startDate
    */
    public String getStartDate() {
        return startDate;
    }

    /**
    * @return the endDate
    */
    public String getEndDate() {
        return endDate;
    }

    /**
    * @return the noOfGames
    */
    public int getNoOfGames() {
        return noOfGames;
    }

    /**
    * @param name the name of this Championship
    * @return this Championship
    */
    public Championship setName(final String name) {
        this.name = name;
        return this;
    }

    /**
    * @param prizePool the prizePool of this Championship
    * @return this Championship
    */
    public Championship setPrizePool(final Float prizePool) {
        this.prizePool = prizePool;
        return this;
    }

    /**
    * @param startDate the startDate of this Championship
    * @return this Championship
    */
    public Championship setStartDate(final String startDate) {
        this.startDate = startDate;
        return this;
    }

    /**
    * @param endDate the endDate of this Championship
    * @return this Championship
    */
    public Championship setEndDate(final String endDate) {
        this.endDate = endDate;
        return this;
    }

    /**
    * @param noOfGames the noOfGames of this Championship
    * @return this Championship
    */
    public Championship setNoOfGames(final int noOfGames) {
        this.noOfGames = noOfGames;
        return this;
    }

    @Override
    public String toString() {
        return "Championship{" +
           "name='" + name + '\'' +
           ", prizePool='" + prizePool + '\'' +
           ", startDate='" + startDate + '\'' +
           ", endDate='" + endDate + '\'' +
           ", noOfGames='" + noOfGames + '\'' +
           '}';
    }

    public Championship updateValues(final Championship championship) {
        return this
            .setName(championship.name)
            .setPrizePool(championship.prizePool)
            .setStartDate(championship.startDate)
            .setEndDate(championship.endDate)
            .setNoOfGames(championship.noOfGames);
    }

}
