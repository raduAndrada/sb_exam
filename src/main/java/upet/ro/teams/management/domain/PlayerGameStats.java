
package upet.ro.teams.management.domain;

import java.util.Collection;
import java.time.Instant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.validation.constraints.NotNull;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import javax.persistence.Entity;

@Entity
public class PlayerGameStats extends AbstractAuditingEntity {

    public int efficiency;

    public int noOfGoals;

    public int noOfFaults;

    public int noOfAssists;

    public int noOfUnprovokedMistakes;

    public int minutesPlayed;

    @OneToOne
    public Game game;

    @OneToOne
    public Player player;

    /**
    * @return the efficiency
    */
    public int getEfficiency() {
        return efficiency;
    }

    /**
    * @return the noOfGoals
    */
    public int getNoOfGoals() {
        return noOfGoals;
    }

    /**
    * @return the noOfFaults
    */
    public int getNoOfFaults() {
        return noOfFaults;
    }

    /**
    * @return the noOfAssists
    */
    public int getNoOfAssists() {
        return noOfAssists;
    }

    /**
    * @return the noOfUnprovokedMistakes
    */
    public int getNoOfUnprovokedMistakes() {
        return noOfUnprovokedMistakes;
    }

    /**
    * @return the minutesPlayed
    */
    public int getMinutesPlayed() {
        return minutesPlayed;
    }

    /**
    * @return the game
    */
    public Game getGame() {
        return game;
    }

    /**
    * @return the player
    */
    public Player getPlayer() {
        return player;
    }

    /**
    * @param efficiency the efficiency of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setEfficiency(final int efficiency) {
        this.efficiency = efficiency;
        return this;
    }

    /**
    * @param noOfGoals the noOfGoals of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setNoOfGoals(final int noOfGoals) {
        this.noOfGoals = noOfGoals;
        return this;
    }

    /**
    * @param noOfFaults the noOfFaults of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setNoOfFaults(final int noOfFaults) {
        this.noOfFaults = noOfFaults;
        return this;
    }

    /**
    * @param noOfAssists the noOfAssists of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setNoOfAssists(final int noOfAssists) {
        this.noOfAssists = noOfAssists;
        return this;
    }

    /**
    * @param noOfUnprovokedMistakes the noOfUnprovokedMistakes of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setNoOfUnprovokedMistakes(final int noOfUnprovokedMistakes) {
        this.noOfUnprovokedMistakes = noOfUnprovokedMistakes;
        return this;
    }

    /**
    * @param minutesPlayed the minutesPlayed of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setMinutesPlayed(final int minutesPlayed) {
        this.minutesPlayed = minutesPlayed;
        return this;
    }

    /**
    * @param game the game of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setGame(final Game game) {
        this.game = game;
        return this;
    }

    /**
    * @param player the player of this PlayerGameStats
    * @return this PlayerGameStats
    */
    public PlayerGameStats setPlayer(final Player player) {
        this.player = player;
        return this;
    }

    @Override
    public String toString() {
        return "PlayerGameStats{" +
           "efficiency='" + efficiency + '\'' +
           ", noOfGoals='" + noOfGoals + '\'' +
           ", noOfFaults='" + noOfFaults + '\'' +
           ", noOfAssists='" + noOfAssists + '\'' +
           ", noOfUnprovokedMistakes='" + noOfUnprovokedMistakes + '\'' +
           ", minutesPlayed='" + minutesPlayed + '\'' +
           ", game='" + game + '\'' +
           ", player='" + player + '\'' +
           '}';
    }

    public PlayerGameStats updateValues(final PlayerGameStats playerGameStats) {
        return this
            .setEfficiency(playerGameStats.efficiency)
            .setNoOfGoals(playerGameStats.noOfGoals)
            .setNoOfFaults(playerGameStats.noOfFaults)
            .setNoOfAssists(playerGameStats.noOfAssists)
            .setNoOfUnprovokedMistakes(playerGameStats.noOfUnprovokedMistakes)
            .setMinutesPlayed(playerGameStats.minutesPlayed)
            .setGame(playerGameStats.game)
            .setPlayer(playerGameStats.player);
    }

}
