
package upet.ro.teams.management.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.quarkus.hibernate.orm.panache.PanacheEntity;



import javax.persistence.MappedSuperclass;
import java.time.Instant;

/**
 * Entity abstract class
 */
@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractAuditingEntity extends PanacheEntity {

    @JsonIgnore
    public Instant createdDate = Instant.now();

    @JsonIgnore
    public Instant modifiedAt = Instant.now();

    @JsonIgnore
    public String createdBy;

    @JsonIgnore
    public String lastEditedBy;

    /**
     * @return creation date
     */
    public Instant getCreatedDate() {
        return createdDate;
    }

    /**
     * Setter for creation date
     * @param createdDate  creation date
     * @return this entity
     */
    public AbstractAuditingEntity setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    /**
     * @return date of last change
     */
    public Instant getLastEditedDate() {
        return modifiedAt;
    }

    /**
     * Setter for modify at
     * @param modifiedAt modification time
     * @return this entity
     */
    public AbstractAuditingEntity setModifiedAt(Instant modifiedAt) {
        this.modifiedAt = modifiedAt;
        return this;
    }

    /**
     * @return the creator of the entity
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter for created
     * @param createdBy the creator of the entity
     * @return this entity
     */
    public AbstractAuditingEntity setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
     * @return get the author of the last change
     */
    public String getLastEditedBy() {
        return lastEditedBy;
    }

    /**
     * Setter for last change author
     * @param lastModifiedBy name of the last change's author
     * @return this entity
     */
    public AbstractAuditingEntity setLastEditedBy(String lastModifiedBy) {
        this.lastEditedBy = lastModifiedBy;
        return this;
    }

}

