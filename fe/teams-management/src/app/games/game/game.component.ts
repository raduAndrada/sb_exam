import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Championship, Game, Team } from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
  providers: [RestService],
})
export class GameComponent implements OnInit {
  championshipList: Championship[];
  teamList: Team[];
  onCreate = true;
  href = '';
  gameUrl = '/api/game';
  selectedHostTeamId: number;
  selectedGuestTeamId: number;
  selectedChampionshipId: number;

  toAdd: Game = {
    guestTeam: null,
    hostTeam: null,
    score: '',
    gameNumber: 0,
    datetime: '',
    championship: null,
  };

  constructor(
    private route: ActivatedRoute,
    private championshipService: RestService<Championship>,
    private gameService: RestService<Game>,
    private teamService: RestService<Team>,
    private router: Router
  ) {
    this.href = this.router.url;
    if (this.href.indexOf('update') !== -1) {
      this.onCreate = false;
      const gameId = this.href.replace(/[^0-9]/g, '');
      this.gameService
        .getById(+gameId, this.gameUrl)
        .subscribe((currentGame: Game) => {
          this.toAdd = currentGame;
        });
    }
    const championshipUrl = '/api/championship';
    this.championshipService
      .getList(championshipUrl)
      .subscribe((championshipList: Championship[]) => {
        this.championshipList = championshipList;
      });
    const teamUrl = '/api/team';
    this.teamService.getList(teamUrl).subscribe((teamList: Team[]) => {
      this.teamList = teamList;
    });
  }

  ngOnInit(): void {}

  create() {
    this.toAdd.guestTeam = this.teamList.filter(
      (t) => t.id === +this.selectedGuestTeamId
    )[0];
    this.toAdd.hostTeam = this.teamList.filter(
      (t) => t.id === +this.selectedHostTeamId
    )[0];
    this.toAdd.championship = this.championshipList.filter(
      (c) => c.id === +this.selectedChampionshipId
    )[0];
    const added = this.gameService
      .create(this.toAdd, this.gameUrl)
      .subscribe((added) => {
        console.log(added);
        this.router.navigate(['games']);
      });
  }
}
