import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Championship, Game, Team, TeamGameStats } from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';
import { TeamGameStatsService } from './TeamGameStatsService.service';

@Component({
  selector: 'app-game-stats',
  templateUrl: './game-stats.component.html',
  styleUrls: ['./game-stats.component.scss'],
  providers: [RestService, TeamGameStatsService],
})
export class GameStatsComponent implements OnInit {
  teamList: Team[] = [];
  onCreate = true;
  href = '';
  teamGameStatsUrl = '/api/team-game-stats';
  gameUrl = '/api/game';
  selectedTeamId: number;

  toAdd: TeamGameStats = {
    team: null,
    mvp: '',
    efficiency: 0,
    goalkeeperEfficiency: 0,
    fastbreakGoals: 0,
    ballPossession: 0,
    game: null,
  };

  game: Game = {
    guestTeam: null,
    hostTeam: null,
    score: '',
    gameNumber: 0,
    datetime: '',
    championship: null,
  };

  constructor(
    private route: ActivatedRoute,
    private gameService: RestService<Game>,
    private teamGameStatsService: TeamGameStatsService,
    private router: Router
  ) {
    this.href = this.router.url;
    var startIdx = this.href.indexOf('gameId/');
    const gameId = this.href.substring(startIdx + 7, startIdx + 8);
    if (this.href.indexOf('update') !== -1) {
      this.onCreate = false;
      startIdx = this.href.indexOf('teamId/');
      const teamId = this.href.substring(startIdx + 7, startIdx + 8);
      this.teamGameStatsService
        .getByTeamAndGameId(+teamId, +gameId, this.teamGameStatsUrl)
        .subscribe((current: TeamGameStats) => {
          this.toAdd = current;
          console.log(this.toAdd);
        });
    } else {
      this.gameService
        .getById(+gameId, this.gameUrl)
        .subscribe((currentGame: Game) => {
          this.game = currentGame;
          this.teamList.push(this.game.hostTeam);
          this.teamList.push(this.game.guestTeam);
          this.toAdd.game = this.game;
        });
    }
  }

  ngOnInit(): void {}

  create() {
    this.toAdd.team = this.teamList.filter(
      (t) => t.id === +this.selectedTeamId
    )[0];

    const added = this.teamGameStatsService
      .create(this.toAdd, this.teamGameStatsUrl)
      .subscribe((added) => {
        console.log(added);
        this.router.navigate(['games']);
      });
  }
}
