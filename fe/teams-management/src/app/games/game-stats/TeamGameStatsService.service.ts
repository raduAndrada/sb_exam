import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { TeamGameStats } from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';

@Injectable()
export class TeamGameStatsService extends RestService<TeamGameStats> {
  constructor(private http2: HttpClient) {
    super(http2);
  }

  getByTeamAndGameId(
    teamId: number,
    gameId: number,
    url: String
  ): Observable<TeamGameStats> {
    const urlTemp =
      this.baseUrl + url + '/teamId/' + teamId + '/gameId/' + gameId;
    return this.http2.get<TeamGameStats>(urlTemp).pipe(
      tap((_) => console.log(_)),
      catchError(this.handleError<TeamGameStats>('getById'))
    );
  }
}
