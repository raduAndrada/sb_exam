import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Game } from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss'],
  providers: [RestService],
})
export class GameListComponent implements OnInit {
  gameList: Game[] = [];

  constructor(
    private route: ActivatedRoute,
    private gameService: RestService<Game>,
    private router: Router
  ) {
    const gameUrl = '/api/game';
    this.gameService.getList(gameUrl).subscribe((gameList: Game[]) => {
      this.gameList = gameList;
    });
  }

  ngOnInit(): void {}

  addGame() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

  editGame(gameId) {
    this.router.navigate([gameId + '/update'], { relativeTo: this.route });
  }

  addStats(gameId) {
    this.router.navigate(['stats/gameId/' + gameId], {
      relativeTo: this.route,
    });
  }

  editStats(gameId, teamId) {
    this.router.navigate(
      ['stats/update/gameId/' + gameId + '/teamId/' + teamId],
      {
        relativeTo: this.route,
      }
    );
  }
}
