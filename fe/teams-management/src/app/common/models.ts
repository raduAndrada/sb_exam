export interface TeamGameStats {
  efficiency: number;

  fastbreakGoals: number;

  ballPossession: number;

  goalkeeperEfficiency: number;

  mvp: String;

  game: Game;

  team: Team;
}

export interface PlayerGameStats {
  efficiency: number;

  noOfGoals: number;

  noOfFaults: number;

  noOfAssists: number;

  noOfUnprovokedMistakes: number;

  minutesPlayed: number;

  game: Game;

  player: Player;
}

export interface Player {
  name: String;

  phoneNumber: String;

  address: String;

  efficiency: String;

  annualSalary: number;

  position: String;

  team: Team;
}

export interface Game {
  id?: number;

  hostTeam: Team;

  guestTeam: Team;

  datetime: String;

  score: String;

  gameNumber: number;

  championship: Championship;
}

export interface Team {
  id?: number;

  name: String;

  address: String;

  currentPosition: number;

  winrate: String;

  owner: String;

  coach: String;

  championship: Championship;
}

export interface Championship {
  id?: number;

  name: String;

  prizePool: number;

  startDate: String;

  endDate: String;

  noOfGames: number;
}
