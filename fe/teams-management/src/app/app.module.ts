import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChampionshipListComponent } from './championships/championship-list/championship-list.component';
import { ChampionshipComponent } from './championships/championship/championship.component';
import { HttpClientModule } from '@angular/common/http';
import { TeamListComponent } from './teams/team-list/team-list.component';
import { TeamComponent } from './teams/team/team.component';
import { HeaderComponent } from './common/header/header/header.component';
import { GameListComponent } from './games/game-list/game-list.component';
import { GameComponent } from './games/game/game.component';
import { PlayerListComponent } from './players/player-list/player-list.component';
import { GameStatsComponent } from './games/game-stats/game-stats.component';
import { PlayerComponent } from './players/player/player.component';
import { PlayerStatsComponent } from './players/player-stats/player-stats.component';
import { PlayerStatsListComponent } from './players/player-stats-list/player-stats-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ChampionshipListComponent,
    ChampionshipComponent,
    TeamListComponent,
    TeamComponent,
    HeaderComponent,
    GameListComponent,
    GameComponent,
    PlayerListComponent,
    GameStatsComponent,
    PlayerComponent,
    PlayerStatsComponent,
    PlayerStatsListComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
