import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/common/RestService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Championship } from 'src/app/common/models';

@Component({
  selector: 'app-championship',
  templateUrl: './championship.component.html',
  styleUrls: ['./championship.component.scss'],
  providers: [RestService],
})
export class ChampionshipComponent implements OnInit {
  toAdd: Championship = {
    name: '',
    prizePool: 0,
    startDate: '',
    endDate: '',
    noOfGames: 0,
  };

  constructor(
    private route: ActivatedRoute,
    private championshipService: RestService<Championship>,
    private router: Router
  ) {}

  ngOnInit(): void {}

  create() {
    const added = this.championshipService
      .create(this.toAdd, '/api/championship')
      .subscribe((added) => {
        console.log(added);
        this.router.navigate(['championships']);
      });
  }
}
