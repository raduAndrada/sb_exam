import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/common/RestService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Championship } from 'src/app/common/models';

@Component({
  selector: 'app-championship-list',
  templateUrl: './championship-list.component.html',
  styleUrls: ['./championship-list.component.scss'],
  providers: [RestService],
})
export class ChampionshipListComponent implements OnInit {
  championshipList: Championship[];

  constructor(
    private route: ActivatedRoute,
    private championshipService: RestService<Championship>,
    private router: Router
  ) {
    const championshipUrl = '/api/championship';
    this.championshipService
      .getList(championshipUrl)
      .subscribe((championshipList: Championship[]) => {
        this.championshipList = championshipList;
      });
  }

  ngOnInit(): void {}

  addChampionship() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
}
