import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Player, Team } from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';
import { PlayerService } from '../PlayerService.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  providers: [PlayerService, RestService],
})
export class PlayerComponent implements OnInit {
  toAdd: Player = {
    name: '',
    address: '',
    position: '',
    efficiency: '',
    annualSalary: 0,
    phoneNumber: '',
    team: null,
  };
  href = '';
  teamId = 0;

  constructor(
    private route: ActivatedRoute,
    private playerService: PlayerService,
    private teamService: RestService<Team>,
    private router: Router
  ) {
    this.href = this.router.url;
    this.teamId = +this.href.replace(/[^0-9]/g, '');
    this.teamService
      .getById(this.teamId, '/api/team')
      .subscribe((team: Team) => {
        this.toAdd.team = team;
      });
  }

  ngOnInit(): void {}

  create() {
    const added = this.playerService
      .create(this.toAdd, '/api/player')
      .subscribe((added) => {
        console.log(added);
        this.router.navigate(['teams/' + this.teamId + '/players']);
      });
  }
}
