import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Player } from 'src/app/common/models';
import { PlayerService } from '../PlayerService.service';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss'],
  providers: [PlayerService],
})
export class PlayerListComponent implements OnInit {
  playerList: Player[];
  href = '';

  constructor(
    private route: ActivatedRoute,
    private playerService: PlayerService,
    private router: Router
  ) {
    this.href = this.router.url;
    const playerUrl = '/api/player';
    const teamId = this.href.replace(/[^0-9]/g, '');
    this.playerService
      .getByTeamAId(+teamId, playerUrl)
      .subscribe((playerList: Player[]) => {
        this.playerList = playerList;
      });
  }

  ngOnInit(): void {}

  addPlayer() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
  addStats(playerId) {
    this.router.navigate([playerId + '/player-stats'], {
      relativeTo: this.route,
    });
  }
}
