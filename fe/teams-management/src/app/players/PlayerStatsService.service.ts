import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {
  Game,
  Player,
  PlayerGameStats,
  TeamGameStats,
} from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';

@Injectable()
export class PlayerStatsService extends RestService<PlayerGameStats> {
  constructor(private http2: HttpClient) {
    super(http2);
  }

  getByPlayerId(playerId: number, url: String): Observable<PlayerGameStats[]> {
    const urlTemp = this.baseUrl + url + '/playerId/' + playerId;
    return this.http2.get<PlayerGameStats[]>(urlTemp).pipe(
      tap((_) => console.log(_)),
      catchError(this.handleError<PlayerGameStats[]>('getById'))
    );
  }

  getGamesByTeamId(teamId: number, url: String): Observable<Game[]> {
    const urlTemp = this.baseUrl + url + '/teamId/' + teamId;
    return this.http2.get<Game[]>(urlTemp).pipe(
      tap((_) => console.log(_)),
      catchError(this.handleError<Game[]>('getById'))
    );
  }
}
