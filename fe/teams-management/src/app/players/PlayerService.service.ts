import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Player, TeamGameStats } from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';

@Injectable()
export class PlayerService extends RestService<Player> {
  constructor(private http2: HttpClient) {
    super(http2);
  }

  getByTeamAId(teamId: number, url: String): Observable<Player[]> {
    const urlTemp = this.baseUrl + url + '/teamId/' + teamId;
    return this.http2.get<Player[]>(urlTemp).pipe(
      tap((_) => console.log(_)),
      catchError(this.handleError<Player[]>('getById'))
    );
  }
}
