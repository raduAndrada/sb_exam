import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayerGameStats } from 'src/app/common/models';
import { PlayerStatsService } from '../PlayerStatsService.service';

@Component({
  selector: 'app-player-stats-list',
  templateUrl: './player-stats-list.component.html',
  styleUrls: ['./player-stats-list.component.scss'],
  providers: [PlayerStatsService],
})
export class PlayerStatsListComponent implements OnInit {
  playerStatsList: PlayerGameStats[];
  href = '';

  constructor(
    private route: ActivatedRoute,
    private playerStatsService: PlayerStatsService,
    private router: Router
  ) {
    this.href = this.router.url;
    const playerStatsUrl = '/api/player-game-stats';
    var startIdx = this.href.indexOf('players/');
    const playerId = this.href.substring(startIdx + 8, startIdx + 9);
    this.playerStatsService
      .getByPlayerId(+playerId, playerStatsUrl)
      .subscribe((playerStatsList: PlayerGameStats[]) => {
        this.playerStatsList = playerStatsList;
      });
  }

  ngOnInit(): void {}

  addPlayerStats() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
}
