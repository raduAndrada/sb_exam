import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Game, Player, PlayerGameStats, Team } from 'src/app/common/models';
import { RestService } from 'src/app/common/RestService.service';
import { PlayerService } from '../PlayerService.service';
import { PlayerStatsService } from '../PlayerStatsService.service';

@Component({
  selector: 'app-player-stats',
  templateUrl: './player-stats.component.html',
  styleUrls: ['./player-stats.component.scss'],
  providers: [PlayerService, PlayerStatsService],
})
export class PlayerStatsComponent implements OnInit {
  gameList: Game[];
  selectedGameId: number;
  toAdd: PlayerGameStats = {
    efficiency: 0,
    minutesPlayed: 0,
    noOfAssists: 0,
    noOfFaults: 0,
    noOfGoals: 0,
    noOfUnprovokedMistakes: 0,
    game: null,
    player: null,
  };
  href = '';

  constructor(
    private route: ActivatedRoute,
    private playerStatsService: PlayerStatsService,
    private playerService: PlayerService,
    private router: Router
  ) {
    const gameUrl = '/api/game';
    const playerUrl = '/api/player';
    this.href = this.router.url;
    const playerStatsUrl = '/api/player-game-stats';
    var startIdx = this.href.indexOf('teams/');
    const teamId = this.href.substring(startIdx + 6, startIdx + 7);
    startIdx = this.href.indexOf('players/');
    const playerId = this.href.substring(startIdx + 8, startIdx + 9);
    this.playerStatsService
      .getGamesByTeamId(+teamId, gameUrl)
      .subscribe((gameList: Game[]) => {
        this.gameList = gameList;
      });
    this.playerService
      .getById(+playerId, playerUrl)
      .subscribe((player: Player) => {
        this.toAdd.player = player;
      });
  }

  ngOnInit(): void {}

  create() {
    this.toAdd.game = this.gameList.filter(
      (c) => c.id == this.selectedGameId
    )[0];

    const added = this.playerStatsService
      .create(this.toAdd, '/api/player-game-stats')
      .subscribe((added) => {
        console.log(added);
        this.router.navigate(['teams']);
      });
  }
}
