import { Component, OnInit } from '@angular/core';
import { Championship, Team } from 'src/app/common/models';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from 'src/app/common/RestService.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss'],
  providers: [RestService],
})
export class TeamListComponent implements OnInit {
  teamList: Team[];

  constructor(
    private route: ActivatedRoute,
    private teamService: RestService<Team>,
    private router: Router
  ) {
    const teamUrl = '/api/team';
    this.teamService.getList(teamUrl).subscribe((teamList: Team[]) => {
      this.teamList = teamList;
    });
  }

  ngOnInit(): void {}

  addTeam() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
  viewPlayers(teamId) {
    this.router.navigate([teamId + '/players'], { relativeTo: this.route });
  }
}
