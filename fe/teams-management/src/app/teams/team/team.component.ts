import { Component, OnInit } from '@angular/core';
import { Team, Championship } from 'src/app/common/models';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from 'src/app/common/RestService.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
  providers: [RestService],
})
export class TeamComponent implements OnInit {
  championshipList: Championship[];
  selectedChampionshipId: number;
  toAdd: Team = {
    name: '',
    address: '',
    owner: '',
    coach: '',
    winrate: '',
    currentPosition: 0,
    championship: null,
  };

  constructor(
    private route: ActivatedRoute,
    private championshipService: RestService<Championship>,
    private teamService: RestService<Team>,
    private router: Router
  ) {
    const championshipUrl = '/api/championship';
    this.championshipService
      .getList(championshipUrl)
      .subscribe((championshipList: Championship[]) => {
        this.championshipList = championshipList;
      });
  }

  ngOnInit(): void {}

  create() {
    this.toAdd.championship = this.championshipList.filter(
      (c) => c.id == this.selectedChampionshipId
    )[0];

    const added = this.teamService
      .create(this.toAdd, '/api/team')
      .subscribe((added) => {
        console.log(added);
        this.router.navigate(['teams']);
      });
  }
}
