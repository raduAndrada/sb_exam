import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ChampionshipListComponent } from './championships/championship-list/championship-list.component';
import { ChampionshipComponent } from './championships/championship/championship.component';
import { TeamListComponent } from './teams/team-list/team-list.component';
import { TeamComponent } from './teams/team/team.component';
import { GameListComponent } from './games/game-list/game-list.component';
import { GameComponent } from './games/game/game.component';
import { GameStatsComponent } from './games/game-stats/game-stats.component';
import { PlayerListComponent } from './players/player-list/player-list.component';
import { PlayerComponent } from './players/player/player.component';
import { PlayerStatsListComponent } from './players/player-stats-list/player-stats-list.component';
import { PlayerStatsComponent } from './players/player-stats/player-stats.component';

const appRoutes: Routes = [
  {
    path: 'championships',
    component: ChampionshipListComponent,
  },
  { path: 'championships/create', component: ChampionshipComponent },
  {
    path: 'teams',
    component: TeamListComponent,
  },
  { path: 'teams/create', component: TeamComponent },
  { path: 'teams/:id/players/create', component: PlayerComponent },
  {
    path: 'teams/:id/players/:id/player-stats/create',
    component: PlayerStatsComponent,
  },
  {
    path: 'teams/:id/players/:id/player-stats',
    component: PlayerStatsListComponent,
  },
  { path: 'teams/:id/players', component: PlayerListComponent },
  {
    path: 'games',
    component: GameListComponent,
  },
  { path: 'games/create', component: GameComponent },
  { path: 'games/stats/gameId/:gameId', component: GameStatsComponent },
  {
    path: 'games/stats/update/gameId/:gameId/teamId/:teamId',
    component: GameStatsComponent,
  },
  { path: 'games/:id/update', component: GameComponent },
  { path: '', redirectTo: 'championships', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes /*, {useHash: true}*/)],

  exports: [RouterModule],
})
export class AppRoutingModule {}
